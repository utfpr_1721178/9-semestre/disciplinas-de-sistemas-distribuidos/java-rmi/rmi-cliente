package JavaRMICliente.src;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface InterfaceCli extends Remote {

    void echo (String texto) throws RemoteException;

    void menu (String texto) throws RemoteException;

    void menuCotacao (String texto) throws RemoteException;

    void menuTransfer (String texto, String tipo) throws RemoteException;

    void menuPadrao(String texto, int i) throws RemoteException;

    void menuNotificacao(String texto) throws RemoteException;

    String scannerListaTransfer(String texto) throws RemoteException;

}

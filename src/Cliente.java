package src;



import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Cliente {

    private Registry servicoNomes;
    private InterfaceServ serv;
    private CliImpl cli;

    Cliente() throws RemoteException, NotBoundException {
        servicoNomes = LocateRegistry.getRegistry("localhost", 2019);
        serv = (InterfaceServ) servicoNomes.lookup("Transfers");
        cli = new CliImpl(serv);
    }

    public static void main(String[] args) {
        try {
            new Cliente();
        } catch (RemoteException | NotBoundException e) {
            e.printStackTrace();
        }
    }

}

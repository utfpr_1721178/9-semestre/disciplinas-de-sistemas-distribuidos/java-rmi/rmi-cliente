package src;



import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.Scanner;
import java.util.UUID;

public class CliImpl extends UnicastRemoteObject implements JavaRMICliente.src.InterfaceCli {

    private InterfaceServ serv;
    static UUID uuid;

    public CliImpl(InterfaceServ serv) throws RemoteException {
        super();
        this.serv = serv;
        uuid = UUID.randomUUID();
        this.serv.menu(this);
    }

    /**
     * Método que imprime textos recebidos pelo servidor na tela do cliente.
     * @param texto
     */

    @Override
    public void echo(String texto) {
        System.out.println(texto);
    }

    @Override
    public void menu(String texto) throws RemoteException {
        String op = "";
        while(!op.equals("1") && !op.equals("2") && !op.equals("3") && !op.equals("4")
                && !op.equals("5") && !op.equals("6") && !op.equals("7")) {
            echo(texto);
            Scanner scanner = new Scanner(System.in);
            op = scanner.nextLine();
            serv.menuOp(this, uuid.toString(), Integer.parseInt(op));
            if (op.equals("7")){
                System.exit(0);
            }
        }
    }

    @Override
    public void menuCotacao(String texto) throws RemoteException {
        echo(texto);
        Scanner scanner = new Scanner(System.in);
        String cotacao = scanner.nextLine().toLowerCase();
        String[] cotacaoArray = cotacao.split(", ");
        if (cotacaoArray.length == 7){
            serv.adicionaCotacao(this, cotacaoArray, uuid.toString());
        } else {
            serv.menuOp(this, uuid.toString(), 1);
        }
        serv.menu(this);
    }

    @Override
    public void menuTransfer(String texto, String tipo) throws RemoteException {
        echo(texto);
        Scanner scanner = new Scanner(System.in);
        String acao = "";
        if (tipo.equals("ALTERACAO")){
            while (!acao.equals(":q")){
                acao = scanner.nextLine().toLowerCase();
                if (!acao.equals(":q")) {
                    String[] campos = acao.split("[: ]");
                    Transfer transfer = serv.getTransfer(uuid.toString());
                    for (int i = 0; i < campos.length; i++) {
                        if (campos[i].equals("veiculo") || campos[i].equals("veículo")) {
                            transfer.setVeiculo(campos[i + 1]);
                        } else if (campos[i].equals("capacidade") || campos[i].equals("passageiros")) {
                            transfer.setnPassageiros(campos[i + 1]);
                        } else if (campos[i].equals("preco") || campos[i].equals("preço") || campos[i].equals("valor")) {
                            transfer.setPreco(campos[i + 1]);
                        } else if (campos[i].equals("cidade")){
                            transfer.setCidade(campos[i+1]);
                        }
                    }
                    serv.atualizaTransfer(transfer);
                }
            }
        } else {
            acao = scanner.nextLine();
            String[] transfer = acao.split(", ");
            if (transfer.length == 4) {
                serv.adicionaTransfer(this, transfer, uuid.toString());
            } else {
                serv.menuOp(this, uuid.toString(), 2);
            }
        }
        serv.menu(this);
    }

    @Override
    public void menuPadrao(String texto, int i) throws RemoteException {
        echo(texto);
        Scanner scanner = new Scanner(System.in);
        String acao = scanner.nextLine().toLowerCase();
        serv.menuPadrao(acao, i);
    }

    @Override
    public void menuNotificacao(String texto) throws RemoteException {
        echo(texto);
        Scanner scanner = new Scanner(System.in);
        String acao = "";
        while (!acao.equals(":q")){
            acao = scanner.nextLine();
        }
        serv.menu(this);
    }

    @Override
    public String scannerListaTransfer(String texto) throws RemoteException {
        echo(texto);
        Scanner scanner = new Scanner(System.in);
        return (scanner.nextLine());
    }
}
